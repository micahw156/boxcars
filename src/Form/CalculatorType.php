<?php

namespace App\Form;

use App\Service\BoxcarsService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Defines the payout calculator form type.
 */
class CalculatorType extends AbstractType
{
    public function __construct(
        private readonly BoxcarsService $boxcars,
    ) {
    }

    #[\Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $destinations = ['-- select a city --' => ''] + array_flip($this->boxcars->destinations());
        $builder
        ->add('destination', ChoiceType::class, [
            'choices' => $destinations,
        ])
        ->add('submit', SubmitType::class);
    }
}
