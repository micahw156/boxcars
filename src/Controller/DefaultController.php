<?php

namespace App\Controller;

use App\Form\CalculatorType;
use App\Form\ResetHistoryType;
use App\Form\UndoHistoryType;
use App\Service\BoxcarsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DefaultController extends AbstractController
{
    public function __construct(
        private readonly BoxcarsService $boxcars,
    ) {
    }

    #[Route('/', name: 'frontpage')]
    public function index(Request $request): Response
    {
        $route = new \stdClass();
        $route->destination = $this->boxcars->getDestId();

        $form = $this->createForm(CalculatorType::class, $route);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $route = $form->getData();
            $history = $this->boxcars->calculate($route->destination);

            return $this->redirectToRoute('frontpage');
        }
        $history = $this->boxcars->getHistory();

        return $this->render('index.html.twig', [
            'title' => 'Boxcars',
            'form' => $form->createView(),
            'trip' => $this->boxcars->getCurrentPayout(),
            'history' => $history,
        ]);
    }

    #[Route('/undo', name: 'undo')]
    public function undo(Request $request): Response
    {
        $entity = new \stdClass();
        $entity->undo = false;
        $form = $this->createForm(UndoHistoryType::class, $entity);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->boxcars->undoLastHistory();

            return $this->redirectToRoute('frontpage');
        }

        $front = $this->generateUrl('frontpage');

        return $this->render('form.html.twig', [
            'title' => 'Boxcars Undo',
            'page_top' => '<strong>Are you sure you want to undo last destination?</strong>',
            'form' => $form->createView(),
            'page_bottom' => "<a href=\"$front\">Go back to current game.</a>",
        ]);
    }

    #[Route('/reset', name: 'reset')]
    public function reset(Request $request): Response
    {
        $reset = new \stdClass();
        $form = $this->createForm(ResetHistoryType::class, $reset);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $reset = $form->getData();
            $this->boxcars->resetHistory();

            return $this->redirectToRoute('frontpage');
        }

        return $this->render('reset.html.twig', [
            'title' => 'Boxcars Reset',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/grid', name: 'show_grid')]
    public function showGrid(): Response
    {
        $payouts = [];
        $destinations = array_keys($this->boxcars->destinations());
        foreach ($destinations as $prev) {
            $row = [];
            foreach ($destinations as $next) {
                $row[$next] = ($next !== $prev)
                ? $this->boxcars->getPayout($prev, $next)
                : ' ';
            }
            $payouts[$prev] = $row;
        }

        return $this->render('grid.html.twig', [
            'title' => 'Payout Table',
            'destinations' => $destinations,
            'payouts' => $payouts,
        ]);
    }

    #[Route('/api/{prev}/{next}', name: 'payout_api')]
    public function api($prev, $next): Response
    {
        return $this->json([
            'prev' => $prev,
            'next' => $next,
            'payout' => $this->boxcars->getPayout($prev, $next),
        ]);
    }

    #[Route('/api/payouts', name: 'payouts_api')]
    public function payouts(): Response
    {
        return $this->json($this->boxcars->getPayoutsTable());
    }
}
