<?php

namespace App\Controller;

use App\Service\BoxcarsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Defines the Payouts Editor form.
 */
class PayoutEditorController extends AbstractController
{
    private readonly SessionInterface $session;

    public function __construct(
        private readonly BoxcarsService $boxcars,
    ) {
        $this->session = $boxcars->getSession();
        $this->session->start();
    }

    #[Route('/editor', name: 'payout_editor')]
    public function index(Request $request): Response
    {
        $entity = $this->session->get('payoutEditObject', new \stdClass());
        if (!isset($entity->destination)) {
            $entity->destination = null;
        } else {
            $payouts = $this->boxcars->getPayoutsTable();
            if (isset($payouts->{$entity->destination})) {
                foreach ($payouts->{$entity->destination} as $key => $value) {
                    $entity->$key = $value;
                }
            } else {
                return $this->redirectToRoute('editor_reset');
            }
        }

        $destinations = $this->boxcars->destinations();

        $formBuilder = $this->createFormBuilder($entity)
        ->add('destination', ChoiceType::class, [
            'choices' => ['-- select a city --' => ''] + array_flip($destinations),
            'disabled' => !empty($entity->destination),
            'row_attr' => ['class' => 'payout-editor-destination'],
        ]);
        if (!empty($entity->destination)) {
            foreach ($payouts->{$entity->destination} as $key => $value) {
                $formBuilder->add($key, NumberType::class, [
                    'label' => $destinations[$key],
                    'row_attr' => ['class' => 'payout-editor-row'],
                ]);
            }
        }
        $formBuilder->add('save', SubmitType::class);
        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $form->getData();
            $this->session->set('payoutEditObject', $entity);
            if (isset($payouts)) {
                foreach ($payouts->{$entity->destination} as $key => $value) {
                    $payouts->{$entity->destination}->{$key} = $entity->$key;
                }
                $this->session->set('payoutsTable', $payouts);
            } else {
                return $this->redirectToRoute('payout_editor');
            }
        }

        return $this->render('editor.html.twig', [
            'title' => 'Payout Editor',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/editor/reset', name: 'editor_reset')]
    public function reset(): RedirectResponse
    {
        $this->session->remove('payoutEditObject');

        return $this->redirectToRoute('payout_editor');
    }
}
