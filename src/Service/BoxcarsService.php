<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines the Boxcars Service.
 */
class BoxcarsService
{
    private readonly SessionInterface $session;

    private readonly string $environment;

    public function __construct(
        RequestStack $requestStack,
        KernelInterface $kernel,
    ) {
        $this->session = $requestStack->getSession();
        $this->session->start();
        $this->environment = $kernel->getEnvironment();
    }

    public function getPayout($prev, $next): float
    {
        $payouts = $this->getPayoutsTable();

        return $payouts->$prev->$next ?? $payouts->$next->$prev ?? 0;
    }

    public function calculate($next): array
    {
        $history = $this->getHistory();
        $prev = [] === $history ? null : end($history)['id'];

        if (!empty($next) && ($next !== $prev)) {
            if (!empty($prev)) {
                $payout = $this->getPayout($prev, $next);
                $history[] = [
                    'id' => $next,
                    'name' => $this->destinations()[$next],
                    'payout' => $payout,
                ];
            } else {
                $history[] = [
                    'id' => $next,
                    'name' => $this->destinations()[$next],
                    'payout' => '-',
                ];
            }
            $this->session->set('history', $history);
        }

        return $history;
    }

    public function getDestId(): ?string
    {
        if ($history = $this->session->get('history', [])) {
            $dest = array_pop($history);

            return $dest['id'];
        }

        return null;
    }

    public function getCurrentPayout(): array
    {
        $history = $this->session->get('history', []);
        switch (count($history)) {
            case 0:
                return ['message' => 'Select home city.'];

            case 1:
                return ['message' => 'Select first destination.'];

            default:
                $history = $this->session->get('history', []);
                $dest = array_pop($history);

                return [
                    'name' => $dest['name'],
                    'payout' => $dest['payout'],
                ];
        }
    }

    public function getHistory(): array
    {
        return $this->session->get('history', []);
    }

    public function undoLastHistory(): void
    {
        $history = $this->getHistory();
        array_pop($history);
        $this->session->set('history', $history);
    }

    public function resetHistory(): void
    {
        $this->session->clear();
    }

    public function getPayoutsTable(): object
    {
        if ($this->session->has('payoutsTable')) {
            return $this->session->get('payoutsTable');
        }

        $location = __DIR__ . '/../../data/payouts.json';

        if (!file_exists($location)) {
            exit('Cannot load payouts file.');
        }
        $file = file_get_contents($location);

        return json_decode($file, null, 512, JSON_THROW_ON_ERROR);
    }

    public function destinations(): array
    {
        return [
            'ALB' => 'Albany',
            'ATL' => 'Atlanta',
            'BAL' => 'Baltimore',
            'BIL' => 'Billings',
            'BHM' => 'Birmingham',
            'BOS' => 'Boston',
            'BUF' => 'Buffalo',
            'BUT' => 'Butte',
            'CAS' => 'Casper',
            'CHS' => 'Charleston',
            'CLT' => 'Charlotte',
            'CHT' => 'Chattanooga',
            'CHI' => 'Chicago',
            'CIN' => 'Cincinnati',
            'CLE' => 'Cleveland',
            'COL' => 'Columbus',
            'DAL' => 'Dallas',
            'DEN' => 'Denver',
            'DES' => 'Des Moines',
            'DET' => 'Detroit',
            'ELP' => 'El Paso',
            'FAR' => 'Fargo',
            'FTW' => 'Fort Worth',
            'HOS' => 'Houston',
            'IND' => 'Indianapolis',
            'JAX' => 'Jacksonville',
            'KCY' => 'Kansas City',
            'KNX' => 'Knoxville',
            'LSV' => 'Las Vegas',
            'LRK' => 'Little Rock',
            'LAX' => 'Los Angeles',
            'LVL' => 'Louisville',
            'MEM' => 'Memphis',
            'MIA' => 'Miami',
            'MKE' => 'Milwaukee',
            'MPL' => 'Minneapolis',
            'MOE' => 'Mobile',
            'NVL' => 'Nashville',
            'NOL' => 'New Orleans',
            'NYC' => 'New York',
            'NFK' => 'Norfolk',
            'OAK' => 'Oakland',
            'OKC' => 'Oklahoma City',
            'OMA' => 'Omaha',
            'PHL' => 'Philadelphia',
            'PHX' => 'Phoenix',
            'PGH' => 'Pittsburgh',
            'POC' => 'Pocatello',
            'POR' => 'Portland, ME',
            'PDX' => 'Portland, OR',
            'PUB' => 'Pueblo',
            'RAP' => 'Rapid City',
            'RNO' => 'Reno',
            'RVM' => 'Richmond',
            'SAC' => 'Sacramento',
            'SLC' => 'Salt Lake City',
            'SAS' => 'San Antonio',
            'SAN' => 'San Diego',
            'SFO' => 'San Francisco',
            'SEA' => 'Seattle',
            'SHR' => 'Shreveport',
            'SPK' => 'Spokane',
            'STL' => 'St. Louis',
            'MSP' => 'St. Paul',
            'TPA' => 'Tampa',
            'TUC' => 'Tucumcari',
            'WAS' => 'Washington',
        ];
    }

    public function getSession(): SessionInterface
    {
        return $this->session;
    }
}
